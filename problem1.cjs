function extract_info(value_pairs, carId){

    let arr = [];

    if (value_pairs == undefined || value_pairs.length == 0 ){

        return arr
    
    }
    
    else{

        for (let key in value_pairs){

            if (value_pairs[key].id == carId){
                // return `Car ${value_pairs[key].id} is a ${value_pairs[key].car_year} ${value_pairs[key].car_model}`
                return value_pairs[key]
            }
        }

        return arr;
    }
}

module.exports = extract_info; 