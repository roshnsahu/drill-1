const inventory = require('../cars.cjs')
const extract_info = require('../problem4.cjs')

test('testing', () => {
    expect(extract_info(inventory)).toContain(2001);
});

test('testing', () => {
    expect(extract_info(inventory)).toContain(1995);
});

test('testing', () => {
    expect(extract_info(inventory)).toContain(1991);
});

test('testing', () => {
    expect(extract_info(inventory)).toContain(1996);
});