const inventory = require('../cars.cjs');
const extract_info = require('../problem2.cjs');

const result = 'Last car is a Lincoln Town Car';
const result1 = 'Last car is a Lincoln Town CaR';
const result2 = 'Last car is a Lincoln Town Car Car';

test('testing', () => {
    expect(extract_info(inventory)).toEqual(result);
})

test('testing', () => {
    expect(extract_info(inventory)).toBe(result1);
})

test('testing', () => {
    expect(extract_info(inventory)).toEqual(result2);
})

test('testing', () => {
    expect(extract_info(inventory)).not.toBeNull();
})