const inventory = require('../cars.cjs')
const extract_info = require('../problem3.cjs')

test('testing', () => {
    expect(extract_info(inventory)).toContain('Yukon');
});

test('testing', () => {
  expect(extract_info(inventory)).toContain('Town Truck');
});

test('testing', () => {
  expect(extract_info(inventory)).toContain('Prizm');
});